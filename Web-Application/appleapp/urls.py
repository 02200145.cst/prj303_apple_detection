from django import views
# from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from .import views


urlpatterns = [
    path('',views.index,name="index"),
    path('about/',views.about,name="about"),
    path('camera/',views.camera,name="camera"),
    path('viewhistory/',views.viewhistory,name="viewhistory"),
    path('predictImage/',views.predictImage, name="predictImage"),
    path('createItem/',views.createItem,name='createItem'),
    path('clear_history/', views.clear_history, name = "clear_history"),
    path('uploadpredictImage/',views.uploadpredictImage, name="uploadpredictImage"),
]

urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)