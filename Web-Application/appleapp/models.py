# Create your models here.
from contextlib import nullcontext
from django.db import models

class history(models.Model):
    serial_number = models.BigAutoField(primary_key=True)
    date_time = models.DateTimeField(auto_now= True)
    image = models.ImageField(null = True, blank = True)
    results = models.TextField()
